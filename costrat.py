from tensorflow.contrib.distributions import Chi2, Normal
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale, minmax_scale
import numpy as np
import pandas as pd
import tensorflow as tf
import functools


def doublewrap(function):
    """
    A decorator decorator, allowing to use the decorator to be used without
    parentheses if not arguments are provided. All arguments must be optional.
    """
    @functools.wraps(function)
    def decorator(*args, **kwargs):
        if len(args) == 1 and len(kwargs) == 0 and callable(args[0]):
            return function(args[0])
        else:
            return lambda wrapee: function(wrapee, *args, **kwargs)
    return decorator


@doublewrap
def define_scope(function, scope=None, *args, **kwargs):
    """
    A decorator for functions that define TensorFlow operations. The wrapped
    function will only be executed once. Subsequent calls to it will directly
    return the result so that operations are added to the graph only once.
    The operations added by the function live within a tf.variable_scope(). If
    this decorator is used with arguments, they will be forwarded to the
    variable scope. The scope name defaults to the name of the wrapped
    function.
    """
    attribute = '_cache_' + function.__name__
    name = scope or function.__name__
    @property
    @functools.wraps(function)
    def decorator(self):
        if not hasattr(self, attribute):
            with tf.variable_scope(name, *args, **kwargs):
                setattr(self, attribute, function(self))
        return getattr(self, attribute)
    return decorator



class JointTransform:
    def __init__(self, model_x, model_y):
        self.model_x = model_x
        self.model_y = model_y
        self.optimize
        self.objective
    @define_scope
    def optimize(self):
        #return tf.train.RMSPropOptimizer(0.0003).minimize(self.objective)
        return tf.train.AdamOptimizer().minimize(self.objective)
        #return self.optimiser.minimize(self.objective)
    @define_scope
    def objective(self):
        pX = self.model_x.probs()
        pY = self.model_y.probs()
        self.pX = pX
        self.pY = pY
        #####################################################
        #  This is the approximation to fishers exact test
        #####################################################
        N = tf.cast(tf.shape(pX)[0], tf.float64)
        a = tf.reduce_sum(pX)
        b = tf.reduce_sum(pY)
        k = tf.reduce_sum(tf.multiply(pX, pY))
        mu = a*b/N
        var = (a*b*(N-a)*(N-b))/(N*N*(N-1.)) + 0.000001
        pcorr = tf.cast(1., tf.float64)-Normal(mu, var).cdf(k-1.)
        return pcorr

class Chi2JointTransform(JointTransform):
    def __init__(self, model_x, model_y):
        JointTransform.__init__(self, model_x, model_y)
    @define_scope
    def objective(self):
        pX = self.model_x.probs()
        pY = self.model_y.probs()
        self.pX = pX
        self.pY = pY
        eps = tf.cast(.001, tf.float64)
        _1 = tf.cast(1., tf.float64)
        N = tf.cast(tf.shape(pX)[0], tf.float64) + eps
        a = tf.reduce_sum(pX) + eps
        b = tf.reduce_sum(pY) + eps
        OXY = tf.reduce_sum(tf.multiply(pX, pY))
        EXY = (a/N)*(b/N)*N
        OnXY = tf.reduce_sum(tf.multiply(_1-pX, pY))
        EnXY = ((N-a)/N)*(b/N)*N
        OnXnY = tf.reduce_sum(tf.multiply(_1-pX, _1-pY))
        EnXnY = ((N-a)/N)*((N-b)/N)*N
        OXnY = tf.reduce_sum(tf.multiply(pX, _1-pY))
        EXnY = (a/N)*((N-b)/N)*N
        chistat = ((OXY - EXY)**2)/EXY
        chistat += ((OnXY - EnXY)**2)/EnXY
        chistat += ((OnXnY - EnXnY)**2)/EnXnY
        chistat += ((OXnY - EXnY)**2)/EXnY
        pcorr = _1 - Chi2(tf.cast(1., tf.float64)).cdf(chistat)
        return pcorr


class RBFKern:
    def __init__(self, x_dim):
        init = tf.ones((1, x_dim), dtype=tf.float64)
        self.l = tf.Variable(init, name='lengthscale', dtype=tf.float64, trainable=True) 
    def __call__(self, X, Y):
        a = X / self.l
        b = Y / self.l
        ass = tf.reduce_sum(tf.square(a), 1)
        bss = tf.reduce_sum(tf.square(b), 1)
        r2 = -2 * tf.matmul(a, tf.transpose(b)) + tf.reshape(ass, (-1, 1)) + tf.reshape(bss, (1, -1))
        ex = tf.exp(-r2/2)
        return ex
        

class GPModel:
    def __init__(self, Xsp, x_dim, induce_pts=1, kern=None, init_ind=None):
        # sort out the gaussian model
        mu, var = tf.nn.moments(Xsp, [0], keep_dims=True)
        mu = tf.tile(mu, [induce_pts, 1])
        var = tf.tile(var, [induce_pts, 1])
        init_mu = tf.random_normal((induce_pts, x_dim), dtype=tf.float64)
        self.mu = tf.Variable(init_mu, name='mu', dtype=tf.float64, trainable=True)
        if init_ind is not None:
            sl = tf.gather(Xsp, tf.constant(init_ind))
            self.mu = self.mu.assign(sl)
        else:
            init_mu = tf.where(tf.equal(var, tf.constant(0., tf.float64)), mu, self.mu)
            self.mu = self.mu.assign(init_mu)
        self.y = tf.ones((induce_pts, 1), dtype=tf.float64)
        self.kern = kern
        if kern is None:
            self.kern = RBFKern(x_dim)
        self.Ks = self.kern(Xsp, self.mu)
        self.K = self.kern(self.mu, self.mu)
        
    def probs(self):
        Kinvy = tf.matmul(tf.matrix_inverse(self.K), self.y)
        KsKinvy = tf.matmul(self.Ks, Kinvy)
        return tf.sigmoid(2.*(KsKinvy - .5))
 

class LogisticRegressionModel:
    def __init__(self, Xsp, x_dim, x_reduce_dim=None):
        if x_reduce_dim is None:
            self.reduce_dim = False
            self.Wx = tf.Variable(tf.random_normal((x_dim, 1), dtype=tf.float64), name='Wx', dtype=tf.float64, trainable=True)
        else:
            self.reduce_dim = True
            self.Wx = tf.Variable(tf.random_normal((x_reduce_dim, 1), dtype=tf.float64), name='Wx', dtype=tf.float64, trainable=True)
            self.Rx = tf.Variable(tf.random_normal((x_dim, x_reduce_dim), dtype=tf.float64), name='Rx', dtype=tf.float64, trainable=True)
        self.bx = tf.Variable(tf.random_normal((1, 1), dtype=tf.float64), name='bx', dtype=tf.float64, trainable=True)
        self.Xsp = Xsp
    def probs(self):
        if self.reduce_dim:
            XWx = tf.matmul(self.Xsp, tf.matmul(self.Rx,self.Wx))
        else:
            XWx = tf.matmul(self.Xsp, self.Wx)
        pX = tf.sigmoid(XWx + self.bx)
        return pX


from scipy.stats import chi2_contingency
from kmedoids import kMedoids
from sklearn.metrics.pairwise import pairwise_distances
class KMedSubGroup:
    def __init__(self):
        pass
    def find_clust(self, Xsp, Ysp):
        minp = 1.
        Dx = pairwise_distances(Xsp, metric='euclidean')
        Dy = pairwise_distances(Ysp, metric='euclidean')
        for ix in range(2, 20):
            try:
                Mx, Cx_ = kMedoids(Dx, ix)
                Cx = np.zeros((Xsp.shape[0], 1))
                for c in Cx_:
                    for i in Cx_[c]:
                        Cx[i] = c
                for iy in range(2, 20):
                    try:
                        My, Cy_ = kMedoids(Dy, iy)
                        Cy = np.zeros((Xsp.shape[0], 1))
                        for c in Cy_:
                            for i in Cy_[c]:
                                Cy[i] = c
                        
                        for cx in np.unique(Cx):
                            for cy in np.unique(Cy):
                                cont_table = np.zeros((2, 2))
                                cont_table[0, 0] = np.logical_and(Cx == cx, Cy == cy).sum()
                                cont_table[0, 1] = np.logical_and(Cx == cx, Cy != cy).sum()
                                cont_table[1, 0] = np.logical_and(Cx != cx, Cy == cy).sum()
                                cont_table[1, 1] = np.logical_and(Cx != cx, Cy != cy).sum()
                                chi2, p, dof, e = chi2_contingency(cont_table)
                                if minp > p:
                                    minp = p
                                    minCx = Cx == cx
                                    minCy = Cy == cy
                    except:
                        pass
            except:
                pass
        self.minCx = minCx
        self.minCy = minCy
        return minCx, minCy       

