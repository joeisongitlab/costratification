from scipy.io.arff import loadarff
from scipy.stats import norm, fisher_exact
import seaborn as sns
from sklearn.metrics.pairwise import pairwise_distances
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs, load_boston, load_diabetes
import numpy as np
import pandas as pd
from sklearn.preprocessing import scale
from costrat import JointTransform, RBFKern, GPModel, KMedSubGroup, LogisticRegressionModel
def convert(x):
    x = str(x)
    if '>' in x or '<' in x:
        return np.nan
    else:
        try:
            return float(x)
        except:
            return np.nan
dic = {}
for i in range(20):
    dic[i] = convert


def print_test(Xs, Ys, name):
    pmin = 1.4
    for ii in range(10):
        Xsp = tf.placeholder(tf.float64, Xs.shape)
        Ysp = tf.placeholder(tf.float64, Ys.shape)
        xmod = LogisticRegressionModel(Xsp, Xs.shape[1])
        ymod = LogisticRegressionModel(Ysp, Ys.shape[1])
        mod = JointTransform(xmod, ymod) 
        
        sess = tf.Session()
        sess.run(tf.global_variables_initializer())
        oldobj = 99999999999999999999.
        ct = 10
        for i in range(20000):
            obj = sess.run(mod.objective, {Xsp: Xs, Ysp: Ys})
            sess.run(mod.optimize, {Xsp: Xs, Ysp: Ys})
            if np.abs(obj - oldobj) < 0.000000000001:
                ct -=1
            else:
                ct = 10
            if ct == 0:
               break
            oldobj = obj
        px = sess.run(mod.pX, {Xsp: Xs, Ysp: Ys})
        py = sess.run(mod.pY, {Xsp: Xs, Ysp: Ys})
        Cx = px > 0.5
        Cy = py > 0.5
        cont_table = np.zeros((2, 2))
        cont_table[0, 0] = np.logical_and(Cx, Cy).sum()
        cont_table[0, 1] = np.logical_and(Cx, np.logical_not(Cy)).sum()
        cont_table[1, 0] = np.logical_and(np.logical_not(Cx), Cy).sum()
        cont_table[1, 1] = np.logical_and(np.logical_not(Cx), np.logical_not(Cy)).sum()
        #print(cont_table)
        p = fisher_exact(cont_table)[1] 
        if pmin > p:
            pmin = p
            pcont_table = cont_table
    #print(p)
    print('%s & %i & %i & %i & %i & %.2e \\\\' % (name, pcont_table[0, 0], pcont_table[0, 1], pcont_table[1, 0], pcont_table[1, 1], pmin))
  

X, xl = make_blobs(centers=2)
Y, yl = make_blobs(centers=2)
Xs = np.vstack([X[xl == 0, :], X[xl == 1, :]])
Ys = np.vstack([Y[yl == 0, :], Y[yl == 1, :]])
print_test(Xs, Ys, 'Toy example')


data = load_boston()
Xs = scale(data.data)
Ys = data.target
Ys = scale(Ys.reshape((-1, 1)))
print_test(Xs, Ys, 'Boston')


data = load_diabetes()
Xs = scale(data.data)
Ys = data.target
Ys = scale(Ys.reshape((-1, 1)))
print_test(Xs, Ys, 'Diabetes')


data = pd.read_csv('data/abalone.data')
Xs = scale(data.values[:, 1:-1])
Ys = data.values[:, -1]
Ys = scale(Ys.reshape((-1, 1)))
print_test(Xs, Ys, 'Abalone')


data = pd.read_csv('data/energyhousing.data.csv')
Xs = scale(data.values[:, :8])
Ys = scale(data.values[:, -2:])
print_test(Xs, Ys, 'Energy housing')


fields = ['Co', 'Cr', 'Ni', 'Zn', 'Cd', 'Cu', 'Pb']
data = pd.read_csv('data/jura.csv', converters=dic)
data = data[fields].dropna()
Xs = scale(data.values[:, :4])
Ys = scale(data.values[:, -3:])
print_test(Xs, Ys, 'Jura')


data, meta = loadarff('data/mtr-datasets/slump.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-3])
Ys = data[:, -3:]
Ys = scale(Ys)
print_test(Xs, Ys, 'Slump')


data, meta = loadarff('data/mtr-datasets/andro.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-6])
Ys = data[:, -6:]
Ys = scale(Ys)
print_test(Xs, Ys, 'Andro')


data, meta = loadarff('data/mtr-datasets/wq.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-14])
Ys = data[:, -14:]
Ys = scale(Ys)
print_test(Xs, Ys, 'Water quality')


data, meta = loadarff('data/mtr-datasets/atp1d.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-6])
Ys = data[:, -6:]
Ys = scale(Ys)
print_test(Xs, Ys, 'ATP (1d)')


data, meta = loadarff('data/mtr-datasets/atp7d.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-6])
Ys = data[:, -6:]
Ys = scale(Ys)
print_test(Xs, Ys, 'ATP (7d)')


data, meta = loadarff('data/mtr-datasets/edm.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-2])
Ys = data[:, -2:]
Ys = scale(Ys)
print_test(Xs, Ys, 'EDM')


data, meta = loadarff('data/mtr-datasets/scm1d.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-16])
Ys = data[:, -16:]
Ys = scale(Ys)
print_test(Xs, Ys, 'SCM (1d)')


data, meta = loadarff('data/mtr-datasets/scm20d.arff')
data = np.array(data[meta.names()].tolist(), dtype=np.float64)
Xs = scale(data[:, :-16])
Ys = data[:, -16:]
Ys = scale(Ys)
print_test(Xs, Ys, 'SCM (20d)')





